import json
import os
from pymongo import MongoClient
import redis

MONGO_COLLECTION_NAME = os.getenv('MONGO_COLLECTION_NAME', 'api_data')
MONGO_DB_NAME = os.getenv('MONGO_DB_NAME', 'main')
MONGO_IP = os.getenv('MONGO_SERVICE_HOST')
MONGO_PORT = os.getenv('MONGO_PORT', 27017)
REDIS_IP = os.getenv('REDIS_SERVICE_HOST')
REDIS_PORT = os.getenv('REDIS_PORT', 6379)
REDIS_DB_NUM = os.getenv('REDIS_DB_NUM', 0)
REDIS_KEY = os.getenv('REDIS_KEY', 'main')


def check_env_vars():
    for global_var in [REDIS_IP, MONGO_IP]:
        if not global_var:
            raise KeyError(f'Missing environment variable: {global_var}')


def connect_to_redis():
    return redis.Redis(host=REDIS_IP, port=REDIS_PORT, db=REDIS_DB_NUM)


def connect_to_mongodb():
    return MongoClient(f"mongodb://{MONGO_IP}")


def read_from_redis(redis_client):
    return redis_client.blpop(REDIS_KEY)


def transform_data(data):
    return json.loads(data[1].decode())


def write_to_db(mongo_client, data):
    mongo_client[MONGO_DB_NAME][MONGO_COLLECTION_NAME].insert_one(data)


if __name__ == '__main__':
    check_env_vars()
    REDIS_CLIENT = connect_to_redis()
    MONGO_CLIENT = connect_to_mongodb()
    while True:
        REDIS_DATA = read_from_redis(REDIS_CLIENT)
        REDIS_DATA = transform_data(REDIS_DATA)
        write_to_db(MONGO_CLIENT, REDIS_DATA)
