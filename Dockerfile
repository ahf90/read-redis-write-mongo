FROM python:3.8-slim-buster
ADD main.py /main.py
RUN pip install -r requirements.txt
CMD ["/main.py"]
ENTRYPOINT ["python"]